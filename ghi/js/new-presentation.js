// // indow.addEventListener('DOMContentLoaded', async () => {
// //     const selectTag = document.getElementById('conference');

// //     const url = 'http://localhost:8000/api/conferences/';
// //     const response = await fetch(url);
// //     if (response.ok) {
// //       const data = await response.json();

// //       for (let conference of data.conferences) {
// //         const option = document.createElement('option');
// //         option.value = conference.href;
// //         option.innerHTML = conference.name;
// //         selectTag.appendChild(option);
// //       }
// //     }

// //   });
// //         const formTag = document.getElementById("create-presentation-form")
// //         formTag.addEventListener("submit", async (event) => {
// //             event.preventDefault();
// //             let formData = new FormData(formTag);
// //             let dataObject = Object.fromEntries(formData);
// //             let json = JSON.stringify(dataObject);
// //             console.log(json)

// //             const option = document.getElementById("option");
// //             const conf_href = option.value
// //             const presentationUrl = `http://localhost:8000${conf.conference}presentations/`;
// //             const fetchConfig = {
// //                 method: "post",
// //                 body: json,
// //                 Headers: {
// //                     "Content-Type": "application/json",
// //                 }
// //             };
// //             const response = await fetch(presentationUrl, fetchConfig);
// //             if (response.ok) {
// //                 formTag.reset();
// //                 const newPresentation = await response.json();
// //                 console.log(newPresentation);
// //             }
// //         });
// function warning(msg) {
//   return `
//   <div class="alert alert-warning" role="alert">
//   Sorry nothing is showing! Try again.
//   </div>`
// }

// window.addEventListener('DOMContentLoaded', async () => {
//   const url = 'http://localhost:8000/api/conferences/';

//   try {
//       const response = await fetch(url);

//       if (!response.ok)   {
//           throw new Error('Response not ok')
//       } else {
//           const data = await response.json();

//           const selectTag = document.getElementById('conference');
//           for (let conference of data.conferences) {
//               const option = document.createElement('option');
//               option.value = conference.id;
//               option.innerHTML = conference.name;
//               selectTag.appendChild(option);
//           }
//           const formTag = document.getElementById('create-presentation-form');
//           formTag.addEventListener('submit', async event => {
//               event.preventDefault();
//               const formData = new FormData(formTag);
//               const c_id = selectTag.options[selectTag.selectedIndex].value;
//               const json = JSON.stringify(Object.fromEntries(formData));
//               const conferenceUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
//               const fetchConfig = {
//                   method: "POST",
//                   body: json,
//                   headers: {
//                   'Content-Type': 'application/json',
//                   },
//               };
//               const response = await fetch(conferenceUrl, fetchConfig);
//               if (response.ok) {
//                   formTag.reset();
//                   const newConference = await response.json();
//               };
//           })
//       }
//   } catch (e) {
//       console.error('error', e);
//       const alert = warning(e.msg)
//       const row = document.querySelector(".form-control")
//       row.innerHTML = alert + row.innerHTML;
//       }

// });
function warning(msg) {
    return `
    <div class="alert alert-warning" role="alert">
    Sorry nothing is showing! Try again.
    </div>`
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();

      const selectTag = document.getElementById('conference');
      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.id;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
    }

    const formTag = document.getElementById('create-presentation-form');
    const selectTag = document.getElementById('conference');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const conferenceId = selectTag.options[selectTag.selectedIndex].value;
      const locationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
      }
    });
  });
